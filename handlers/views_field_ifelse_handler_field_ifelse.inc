<?php

/**
 * A handler to provide a if-elseif-else field to compare rendered text
 * against specific values.
 *
 * @ingroup views_field_handlers
 */
class views_field_ifelse_handler_field_ifelse extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    $single = array(
      'left' => array('default' => ''),
      'op' => array('default' => NULL),
      'right' => array('default' => ''),
      'text' => array('default' => '', 'translatable' => TRUE),
    );
    $count = count($this->options['if-elseif']);

    $options['if-elseif'] = array_fill(0, $count + 1, $single);
    $options['else'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $count = count($this->options['if-elseif']);
    $specs = $this->options['if-elseif'];
    $specs[] = array(
      'left' => '',
      'op' => '',
      'right' => '',
      'text' => '',
    );
    $op_options = $this->operator_options();
    // If ... Else If ...
    foreach ($specs as $default) {
      $form['if-elseif'][] = array(
        '#type' => 'fieldset',
        '#title' => empty($form['if-elseif'][0]) ? t('If') : t('Else if'),
        '#collapsible' => TRUE,
        'left' => array(
          '#title' => t('Left'),
          '#type' => 'textfield',
          '#default_value' => $default['left'],
        ),
        'op' => array(
          '#title' => t('Op'),
          '#type' => 'select',
          '#default_value' => $default['op'],
          '#options' => $op_options,
        ),
        'right' => array(
          '#title' => t('Right'),
          '#type' => 'textfield',
          '#default_value' => $default['right'],
        ),
        'text' => array(
          '#title' => t('Then'),
          '#type' => 'textarea',
          '#default_value' => $default['text'],
        ),
        'remove' => array(
          '#title' => t('Remove'),
          '#type' => 'checkbox',
          '#default_value' => FALSE,
        ),
      );
    }
    // Else
    $form['else'] = array(
      '#title' => t('Else'),
      '#type' => 'textarea',
      '#default_value' => $this->options['else'],
    );

    // Copy the replacement patterns help.
    $form['help'] = $form['alter']['help'];
    unset($form['help']['#dependency']);
    unset($form['help']['#process']);
  }

  /**
   * Cleanup items on submission by removing empty ones.
   */
  function options_submit(&$form, &$form_state) {
    $options = &$form_state['values']['options'];
    $control_specs = array();
    foreach ($options['if-elseif'] as $spec) {
      if (empty($spec['remove']) && drupal_strlen($spec['left'])) {
        unset($spec['remove']);
        $control_specs[] = $spec;
      }
    }
    $options['if-elseif'] = $control_specs;
    parent::options_submit($form, $form_state);
  }

  /**
   * Render the single if-elseif clause that matches or 'else'.
   */
  function render($values) {

    // If and else-if
    foreach ($this->options['if-elseif'] as $spec) {
      if ($this->evaluate_spec($spec)) {
        return $this->replace_with_tokens($spec['text']);
      }
    }

    // Return the text, so the code never thinks the value is empty.
    return $this->replace_with_tokens($this->options['else']);
  }

  /**
   * Evaluate the clause of one single specification.
   *
   * @return boolean
   *   either TRUE or FALSE
   */
  function evaluate_spec($spec) {
    $left = $this->replace_with_tokens($spec['left']);
    $right = $this->replace_with_tokens($spec['right']);

    $ops = $this->operators();
    // Check if operator method exists.
    if (!empty($ops[$spec['op']]['method']) && method_exists($this, $ops[$spec['op']]['method'])) {
      return (bool) $this->{$ops[$spec['op']]['method']}($left, $right, $spec['op']);
    }
    return FALSE;
  }

  /**
   * A simple replacement wrapper for any text.
   *
   * @see views_handler_field::render_altered()
   */
  function replace_with_tokens($text) {
    $tokens = $this->get_render_tokens(array());
    // Filter this right away as our substitutions are already sanitized.
    $value = filter_xss_admin($text);
    $value = strtr($value, $tokens);

    return $value;
  }


  /**
   * Override get_render_tokens() to generate it allways with an empty item,
   * as this field is just a pseudo field.
   */
  function get_render_tokens($item) {
    return parent::get_render_tokens(array());
  }

  /**
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   */
  function operators() {
    $operators = array(
      '=' => array(
        'title' => t('Is equal to'),
        'short' => t('='),
        'method' => 'op_equal',
        'values' => 1,
      ),
      '!=' => array(
        'title' => t('Is not equal to'),
        'short' => t('!='),
        'method' => 'op_equal',
        'values' => 1,
      ),
      'contains' => array(
        'title' => t('Contains'),
        'short' => t('contains'),
        'method' => 'op_contains',
        'values' => 1,
      ),
      //'word' => array(
      //  'title' => t('Contains any word'),
      //  'short' => t('has word'),
      //  'method' => 'op_word',
      //  'values' => 1,
      //),
      //'allwords' => array(
      //  'title' => t('Contains all words'),
      //  'short' => t('has all'),
      //  'method' => 'op_word',
      //  'values' => 1,
      //),
      //'starts' => array(
      //  'title' => t('Starts with'),
      //  'short' => t('begins'),
      //  'method' => 'op_starts',
      //  'values' => 1,
      //),
      //'not_starts' => array(
      //  'title' => t('Does not start with'),
      //  'short' => t('not_begins'),
      //  'method' => 'op_not_starts',
      //  'values' => 1,
      //),
      //'ends' => array(
      //  'title' => t('Ends with'),
      //  'short' => t('ends'),
      //  'method' => 'op_ends',
      //  'values' => 1,
      //),
      //'not_ends' => array(
      //  'title' => t('Does not end with'),
      //  'short' => t('not_ends'),
      //  'method' => 'op_not_ends',
      //  'values' => 1,
      //),
      //'not' => array(
      //  'title' => t('Does not contain'),
      //  'short' => t('!has'),
      //  'method' => 'op_not',
      //  'values' => 1,
      //),
      //'shorterthan' => array(
      //  'title' => t('Length is shorter than'),
      //  'short' => t('shorter than'),
      //  'method' => 'op_shorter',
      //  'values' => 1,
      //),
      //'longerthan' => array(
      //  'title' => t('Length is longer than'),
      //  'short' => t('longer than'),
      //  'method' => 'op_longer',
      //  'values' => 1,
      //),
    );
    return $operators;
  }

  /**
   * Build strings from the operators() for 'select' options
   */
  function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  function op_equal($left, $right, $op = NULL) {
    switch ($op) {
      case '=':
        return $left == $right;
      case '!=':
        return $left != $right;
      default:
        return FALSE;
    }
  }

  function op_contains($left, $right, $op = NULL) {
    return strpos($left, $right) !== FALSE;
  }

}