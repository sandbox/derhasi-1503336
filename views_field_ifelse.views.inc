<?php

/**
 * @file
 *  Views implementation for an ajax load field.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_field_ifelse_views_data_alter(&$data) {
  $data['views']['ifelse'] = array(
    'title' => t('If-Else'),
    'help' => t('Render text for specific comparisons on rendered fields.'),
    'field' => array(
      'handler' => 'views_field_ifelse_handler_field_ifelse',
    ),
  );
}
